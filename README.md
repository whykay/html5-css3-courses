Handy tool to try out HTML5 and CSS3 code snippets: [http://www.cssdesk.com/](http://www.cssdesk.com/)

## Slides

* [Part 1](https://docs.google.com/presentation/d/15rv9h9LX33sL2T4K1NRKWUcmG27YA-Rpv31PQ-345ew/edit?usp=sharing) - setup and intro to HTML5 and CSS3

* [Part 2]() - More about HTML5 and CSS3
* [Part 3]() - Layout and final exercise to create a cv/profile page

## Exercises

Aside from the exercises here, you can also find Part 3 examples (blog, normalise.css, blog post, floats and positioning) at [http://stephaniefrancis.github.io/codinggrace/](http://stephaniefrancis.github.io/codinggrace/)

You can download exercises via [https://bitbucket.org/whykay/html5-css3-courses/downloads]([https://bitbucket.org/whykay/html5-css3-courses/downloads)